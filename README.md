# Assignment 3 anno 2020-2021

## Componenti del gruppo:
+ 830667 Marco Locatelli
+ 807454 Ilario Rizzi
+ 830190 Francesco Faccioli

# Descrizione
L’applicazione è una cantina virtuale, dove ogni utente può gestire la propria collezione di vini e grappe. Inoltre, si da la possibilità all’utente di avere amici, tale legame permette agli utenti di visitare la cantina degli "amici".

# Esecuzione Applicazione
- Scaricare il file ThirdAssignment-1.0.jar contenuto all’interno della cartella target oppure l’intero repository nel caso si volesse procedere per una installazione pulita con <br />
**mvn clean package**
- Accedere alla cartella contenete il file .jar e da linea di comando eseguire <br />
**java -jar target_ThirdAssignment-1.0.jar** (il nome potrebbe variare, si prega di modificare il comando in base al nome del file .jar senza dipendenze)
- Accedere tramite il browser alla pagina http://localhost:8080/home
- E’ consigliabile accedere utilizzando una mail tra queste (**utente1@gmail.com** / **utente2@gmail.com**) perchè più corpose, oppure questa per il ruolo di admin (**admin@gmail.com**) oppure a quelle presenti all’indirizzo http://localhost:8080/utente, oppure infne registrarsi come nuovo utente.
- Da qui è possibile navigare e eseguire operazioni all’interno della Web App.

**Note**: <br />
Per quanto riguarda il database ovviamente non è necessaria nessuna azione in quanto
l’applicazione si collega autonomamente. <br />
In caso si riscontrino problemi con l’esecuzione del .jar, la versione di java per il packaging da
noi utilizzata è la jdk-15.0.1.
