/**
 */

function hideIfNotLogged(){
	
	var bool = getCookie("Logged");
	//console.log(bool);
	if(bool=="true") {
		showByRemoveClass("welcomeDiv");
		showLoggedUser();
	}else{
		hideButtons();
	}
	
}

function hideButtons(){
	
	$(".action").hide();
	
}

function showLoggedUser(){
	
	var nome = "Ciao, " + getCookie("Name") + " " + getCookie("Lastname") + "!";
	console.log(nome);
	//var cognome = getCookie("Lastname");
	replace("welcomeDiv",nome);
	
}

function hideIfNotAdmin(){
	
	var bool = getCookie("Logged");
	if(bool=="true") {
		showByRemoveClass("welcomeDiv");
		showLoggedUser();
		var role = getCookie("Role");
		if(role == "admin");
		else hideButtons();
	}else{
		hideButtons();
	}
	
}

function updatePlusModalVino(str){
	var res =str.split("-");
	document.getElementById("tipovino").value = res[0];
	document.getElementById('cantina'). value = res[1];
	
}

function updatePlusModalGrappa(str){
	
	var res =str.split("-");
	document.getElementById("tipograppa").value = res[0];
	document.getElementById('cantina'). value = res[1];
	
}

function addCount() {
	  var count = parseInt(document.getElementById("labelBottiglie").value) + 1;
	  document.getElementById("labelBottiglie").value = count;
	}
	
function decreaseCount() {		
	  var count = parseInt(document.getElementById("labelBottiglie").value) -1;
	  if (count < 1){
		  count = 1
	  }
	  document.getElementById("labelBottiglie").value = count;
}

function selectHandle(){
	if(document.getElementById('editSelectNazionalita').value == 'it'){		
		showByRemoveClass('editRegioneDiv');
		hide('editPaeseDiv');
	}else{
		showByRemoveClass('editPaeseDiv');
		hide('editRegioneDiv');
	}	
}

function selectAddHandle(){
	if(document.getElementById('addSelectNazionalita').value == 'it'){		
		showByRemoveClass('addRegioneDiv');
		hide('addPaeseDiv');
	}else{
		showByRemoveClass('addPaeseDiv');
		hide('addRegioneDiv');
	}	
}

/*function adjustFooter(){
	var footerDesc = ("ver. ").concat(localStorage.getItem('build'), " :: &copy; 2020");
	document.getElementById('ver').innerHTML = footerDesc;
}

function printData(id){
	var time = new Date();
	  var timeStr = time.toLocaleTimeString();	
	var date = "Milano - " + timeStr;
	document.getElementById(id).innerHTML = date;
}*/

function popup(id){
	var n = extractId(id);
	var str = "deleteForm";	
	var who = str.concat(n);
	
	$.confirm({
	    title: 'DeviceManagement',
	    content: 'Sei sicuro di voler eliminare?',
	    buttons: {
	        confirm: function () {
	        	document.getElementById(who).submit();
	            $.alert('Eliminato');
	        },
	        cancel: function () {}
	    }
	});
	
	/*var domanda = confirm("Sei sicuro di voler eliminare questo dispositivo?");
	if (domanda == true) {
		document.getElementById(who).submit();
	}*/
}

function extractId(str){
	var id="";
	for(i=0;i<str.length;i++){
		if(str[i]=='0'||str[i]=='1'||str[i]=='2'||str[i]=='3'||str[i]=='4'||str[i]=='5'
			||str[i]=='6'||str[i]=='7'||str[i]=='8'||str[i]=='9'){
			id=id+str[i];
		}
	}
	if(id[0]=='8'&&id[1]=='0'&&id[2]=='8'&&id[3]=='0') return id.substring(4,id.length);
	else return id;
}

function extractIdFromPath(str){
	var id="";
	for(i=0;i<str.length;i++){
		if(str[i]=='i'&&str[i+1]=='d'&&str[i+2]=='='){
			for(j=(i+3);j<str.length;j++){
				if(str[j]!=='&') id = id.concat(str[j]);
				else return id;
			}
		}
	}
	return id;
}

function clear(){
	localStorage.clear();
	setCookie("Authorization","",0);
}

function setCookie(name, value, expiryHours) {
	  var d = new Date();
	  d.setTime(d.getTime() + (expiryHours*60*60*1000));
	  var expires = "expires="+ d.toUTCString();
	  document.cookie = name + "=" + value + ";" + expires + ";path=/";
	}

function getCookie(name) {
	  var nome = name + "=";
	  var decodedCookie = decodeURIComponent(document.cookie);
	  var ca = decodedCookie.split(';');
	  for(var i = 0; i <ca.length; i++) {
	    var c = ca[i];
	    while (c.charAt(0) == ' ') {
	      c = c.substring(1);
	    }
	    if (c.indexOf(nome) == 0) {
	      return c.substring(nome.length, c.length);
	    }
	  }
	  return "";
}

/*function result(){
	var x = document.getElementById("result");
	result.innerHTML = "Mail inviata";
}*/

/*function changeNotifications(){
	axios.post("/home/notifiche").then(response => {
		console.log("Ora le notifiche sono " + response.data);
      })
      .catch((error) => {
        console.log('error' + error);
      });
}*/

/*function changeNotifications(id){
	axios.post("/home/notifiche?id="+id).then(response => {
		console.log("Ora le notifiche " + id + " sono: " + response.data);
      })
      .catch((error) => {
        console.log('error' + error);
      });
}*/

/*function compareStrings(str1,str2) {
    var result = str1.localeCompare(str2);
    return result;
 }

function getNotifiche() {
	axios.get("/home/getnotifiche").then(response => {
		if((compareStrings(response.data,"true"))==0) document.getElementById('tsi').checked=true;		
		else document.getElementById('tsi').checked=false;
		console.log((compareStrings(response.data,"true"))==0);
      })
      .catch((error) => {
        console.log('error' + error);
      });
}*/

function replace(id,data){
	document.getElementById(id).innerHTML = data;
}

function show(id) {
	  var element = document.getElementById(id);
	  if (element.style.display == "none") {
		  element.style.display = "inline-block";
	  } else {
		  element.style.display = "none";
		}
}

function showByRemoveClass(id){
	document.getElementById(id).classList.remove('hidden');
}

function defaultShow(id) {
	  var element = document.getElementById(id);
	  if (element.style.display == "none") {
		  element.style.display = "block";
	  } else {
		  element.style.display = "none";
		}
}

function hide(id){
	var element = document.getElementById(id);
	element.classList.add("hidden");
}

// home richiama sé stessa per arricchire i dati della pagina
//non più utile con gestione tramite cookie

function fixPath(path){
	window.history.pushState(document.state, document.title, path);
}

/*function fixPath(single){
	if(single==true){
		var id = extractId(window.location.href);
		var section = extractSection(window.location.href);
		var where = "/".concat(section,"/",id);
		window.history.pushState(document.state, document.title, where);
	}else{
		var section = extractSection(window.location.href);
		var where = "/".concat(section,"/");
		window.history.pushState(document.state, document.title, where);
	}	
}*/

/*function extractSection(path){
	var indexUser = path.search("user");
	var indexDevice = path.search("device");
	if(indexUser != -1) return "user";
	if(indexDevice != -1) return "device";
}

function adjust(){
	adjustHeader();
	adjustFooter();
}

function adjustHeader(){
	var userLogged = localStorage.getItem('userLogged');
    document.getElementById('userLogged').innerHTML = userLogged;
}

function controlActive(){
	var navItem = extractSection(window.location.href);
	if (navItem < "user" || navItem > "user"){
		var target = document.getElementById('navDevices');
		target.className = (target.className).concat(" active");
	}else{
		var target = document.getElementById('navUsers');
		target.className = (target.className).concat(" active");
	}
}

function switchInput(){
	if ($('#tsi').prop('checked',true)) {
			$('#tsi').prop('checked', false);
		}else{
			$('#tsi').prop('checked', true);
			}
}

function aget(URL){
	axios.get(URL).then(response => {
        document.getElementById('container').innerHTML=response.data;
        fixPath('Users','Utenti','/user/');
      })
      .catch((error) => {
        console.log('error' + error);
      });
}

function controlActive(){
	var where = window.location.href;
	if(where.includes('device')) document.getElementById('navDevices').classList.add('active');
	if(where.includes('user')) document.getElementById('navUsers').classList.add('active');
}

function isValidCf(str){
	var regex = /[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z]/;
	return regex.test(str);
}*/

/*function changeNotifiche() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
            ;
            /*var notifiche = xmlHttp.responseText;
            var str1 = "true";
            var n = str1.localeCompare(notifiche);
        	if (n==0) document.getElementById("tsi").checked=true;
        	else document.getElementById("tsi").checked=false;
        }
    }
    xmlHttp.open("POST", "http://localhost:8080/home/notifiche", true); // true for asynchronous 
    xmlHttp.send();
}*/

/*function doSomething(where) {
const form = document.getElementById('login');
const serialize_form = form => JSON.stringify(
	Array.from(new FormData(form).entries())
		.reduce((m, [ key, value ]) => Object.assign(m, { [key]: value }), {})
	);
const json = serialize_form(form);
console.log(json);
var xhr = new XMLHttpRequest();
xhr.open("POST", where, true);
xhr.setRequestHeader('Content-Type', 'application/json');
xhr.onreadystatechange = function() {
	if (xhr.readyState == 4 && xhr.status == 200){
		var fn = xhr.getResponseHeader("Fn");
		console.log(fn);
		var ln = xhr.getResponseHeader("Ln");
	}
}
xhr.send(json);
}*/