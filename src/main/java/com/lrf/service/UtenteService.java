package com.lrf.service;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.lrf.domain.CantinaProduttrice;
import com.lrf.domain.Grappa;
import com.lrf.domain.Utente;
import com.lrf.domain.UtenteGrappa;
import com.lrf.domain.UtenteVino;
import com.lrf.domain.Vino;
import com.lrf.repository.CantinaProduttriceRepository;
import com.lrf.repository.GrappaRepository;
import com.lrf.repository.UtenteGrappaRepository;
import com.lrf.repository.UtenteRepository;
import com.lrf.repository.UtenteVinoRepository;
import com.lrf.repository.VinoRepository;
import com.lrf.request.WrapperProdottoUtente;

@Service
public class UtenteService {
	
	
	@Autowired
	private UtenteRepository utentiDB;
	
	@Autowired
	private UtenteGrappaRepository utenteGrappaDB;
	
	@Autowired
	private UtenteVinoRepository utentiViniDB;
	
	@Autowired
	private VinoRepository viniDB;

	@Autowired
	private GrappaRepository grappaDB;
	
	@Autowired
	private CantinaProduttriceRepository cantinaDB;
	
	
	/**
	 * Metodo per trovare tutti gli utenti 
	 * @param model Modello UI
	 * @param nome Mail dell'utente
	 * @param role Ruolo dell'utente
	 */
	public void findAll(Model model, String mail, String role) {

		List<Utente> listaUtenti = utentiDB.findAll();
		model.addAttribute("utenti",listaUtenti);
		
		if(role != null && role.equalsIgnoreCase("administrator")) {
			model.addAttribute("admin", true);
		}	
	}
	
	/**
	* Metodo che restituisce un utente
	* @param mail Mail dell'utente
	*/
	public Utente getUtente(Model model, String mail) {
	
		if(utentiDB.existsByMail(mail)) {
			return utentiDB.findByMail(mail);
		}else {
			model.addAttribute("error", true);
			model.addAttribute("message", "Mail inesistente.");
		return null;
		}
	}
	
	/**
	 * Metodo per aggiungere un utente g
	 * @param model Modello UI
	 * @param nome Nome dell'utente
	 * @param cognome Cognome dell'utente
	 * @param mail Mail dell'utente
	 */
	@Transactional
	public void addUtente(Model model, String nome, String cognome, String mail) {
		
		if(!utentiDB.existsByMail(mail)) {
			Utente utente = new Utente(nome,cognome, mail);
			utentiDB.save(utente);
			model.addAttribute("response", "Utente aggiunto correttamente.");
		}else {
			model.addAttribute("error", true);
			model.addAttribute("message", "Mail già utilizzata.");
		}
	}
	
	/**
	 * Metodo per modificare un utente 
	 * @param model Modello UI
	 * @param id ID dell'utente
	 * @param nome Nome dell'utente
	 * @param cognome Cognome dell'utente
	 * @param mail Mail dell'utente
	 */
	@Transactional
	public void updateUtente(Model model, Long id,String nome, String cognome, String mail, String role, String mailCookie) {
		
		Utente utente = utentiDB.findById(id).get();
		if(utente!=null) {
			
				utente.setNome(nome);
				utente.setCognome(cognome);
				if(!utentiDB.existsByMail(mail)) {
					utente.setMail(mail);
				}else {
					model.addAttribute("error", true);
					model.addAttribute("message", "Mail già utilizzata.");
				}
				utentiDB.saveAndFlush(utente);
				model.addAttribute("response","Utente modificato correttamente.");
		}else {
			model.addAttribute("error", true);
			model.addAttribute("message", "Utente non trovato.");
		}
		
		findAll(model, mailCookie, role);
	}
	
	/** 
	 * Metodo per cancellare un utente
	 * @param model Modello UI
	 * @param idUtente ID dell'utente
	 * @param mail Mail dell'utente
	 * @param response Response usata per modificare i cookie
	 * @param request Request http per ottenere i vari cookie
	 */
	@Transactional
	public boolean deleteUtente( Model model,Long idUtente, String mail, HttpServletResponse response, HttpServletRequest request) {
		
		if(utentiDB.existsById(idUtente)) {
			Utente user = utentiDB.findById(idUtente).get();
			for(Utente u: user.getAllUtenti()) {
				u.getAllUtenti().remove(user);
				u.getAmici().remove(user);
			}
			for(Utente u: user.getAmici()) {
				u.getAllUtenti().remove(user);
				u.getAmici().remove(user);
			}
			
			utentiDB.deleteById(idUtente);
			if (user.getMail().equals(mail)) {
				Cookie[] cookieArray = request.getCookies();
				if(cookieArray==null);
				else {
					for(int i=0; i<cookieArray.length; i++) {
						cookieArray[i].setMaxAge(0);
						response.addCookie(cookieArray[i]);
					}
				}
				Cookie cookie = new Cookie("Logged","false");
				response.addCookie(cookie);
			}
			
			model.addAttribute("response","Utente eliminato correttamente.");
		}else {
			model.addAttribute("response","Utente non trovato.");
			return false;
		}
		
		return true;
	}
	
	/** 
	 * Metodo per aggiungere un amico ad un utente
	 * @param idUtente ID dell'utente "amico"
	 * @param mail Mail dell'utente loggato
	 */
	@Transactional
	public boolean addFriend(Long idUtente, String mail) {
		
		Utente user = utentiDB.findByMail(mail);
		Utente nuovoAmico = utentiDB.findById(idUtente).get();
		
		if( user != null && nuovoAmico != null && user != nuovoAmico) {
			List<Utente> amici = user.getAmici();
			if (!amici.contains(nuovoAmico)) {
				amici.add(nuovoAmico);
				user.setAmici(amici);
				utentiDB.save(user);
			}else {
				return false;
			}
		}else {
			return false;
		}
		return true;
		
	}
	
	/** 
	 * Metodo per rimuovere un amico ad un utente
	 * @param idUtente ID dell'utente "amico"
	 * @param mail Mail dell'utente loggato
	 */
	@Transactional
	public void removeFriend(Long idUtente, String mail) {
		
		Utente user = utentiDB.findByMail(mail);
		Utente amico = utentiDB.findById(idUtente).get();
		if (user != null && amico != null) {
			amico.getAmici().remove(user);
			user.getAmici().remove(amico);
			utentiDB.save(user);
			utentiDB.save(amico);
		}
		
	}
	
	/**
	 * Metodo per ottenere i prodotti  un amico ad un utente
	 * @param mail Mail dell'utente loggato
	 * @param model Model 
	 */
	public void viewCantina( String mail, Model model) {

		Utente user = null;
		
		user = utentiDB.findByMail(mail);
		model.addAttribute("isProprietario", true);
		
		if (user != null) {
			
			List<UtenteGrappa> grappeCantina = utenteGrappaDB.findByUtente(user);
			model.addAttribute("grappe", grappeCantina);
			
			List<UtenteVino> viniCantina = utentiViniDB.findByUtente(user);
			model.addAttribute("vini",viniCantina);

			List<Utente> amici1 = user.getAmici();
			
			List<Utente> amici2 = user.getAllUtenti();
			amici1.removeAll(amici2);
			amici2.addAll(amici1);
			model.addAttribute("amici", amici2);		
		}

	}
	
	/** 
	 * Metodo per ottenere i prodotti di una cantina di un amico
	 * @param model Model
	 * @param mail Mail dell'utente loggato
	 * @param id ID dell'utente "amico" 
	 */
	public void viewFriendCantina(Model model,  String mail, Long id) {
		Utente user = null;
		user = utentiDB.findById(id).get();
		model.addAttribute("isProprietario", false);
		model.addAttribute("amico", user);
	
		if (user != null) {	
			List<UtenteGrappa> grappeCantina = utenteGrappaDB.findByUtente(user);
			model.addAttribute("grappe", grappeCantina);
			
			List<UtenteVino> viniCantina = utentiViniDB.findByUtente(user);
			model.addAttribute("vini",viniCantina);
			
			List<Utente> amici1 = user.getAmici();
			
			List<Utente> amici2 = user.getAllUtenti();
			amici1.removeAll(amici2);
			amici2.addAll(amici1);
			model.addAttribute("amici", amici2);		
		}else {
			model.addAttribute("response", "Utente amico non esistente");
		}
	}
	
	/**
	 * Metodo per modificare il numero di bottiglie
	 * @param mail Mail dell'utente loggato
	 * @param prodotto Prodotto da modificare 
	 * */
	@Transactional
	public boolean updateBottiglie(String mail, List<WrapperProdottoUtente> prodotto) {
		
		Utente user = utentiDB.findByMail(mail);
		
		if(user == null) {
			return false;
		}
		
		if (prodotto.get(0).getProdotto().equalsIgnoreCase("vino")) {
			Vino vino = viniDB.findById(Long.parseLong(prodotto.get(0).getIdProdotto())).get();
			
			if(vino==null) {
				return false;
			}
			
			UtenteVino user_has_vino = utentiViniDB.findByUtenteAndAnnoAndVino(
					user,
					Integer.parseInt(prodotto.get(0).getAnno()),
					vino);
			
			if(user_has_vino==null) {
				return false;
			}
			
			user_has_vino.setnBottiglie(Integer.parseInt(prodotto.get(0).getnBottiglie()));
			utentiViniDB.save(user_has_vino);
		}else
			if(prodotto.get(0).getProdotto().equalsIgnoreCase("grappa")) {
				Grappa grappa = grappaDB.findById(Long.parseLong(prodotto.get(0).getIdProdotto())).get();
				
				if(grappa==null) {
					return false;
				}
				
				UtenteGrappa user_has_grappa = utenteGrappaDB.findByUtenteAndAnnoAndGrappa(
						user,
						Integer.parseInt(prodotto.get(0).getAnno()),
						grappa);
				
				if(user_has_grappa==null) {
					return false;
				}
				
				user_has_grappa.setnBottiglie(Integer.parseInt(prodotto.get(0).getnBottiglie()));
				utenteGrappaDB.save(user_has_grappa);
			}
		return true;
		
	}
	
	/**
	 * Metodo per cancellare il prodotto
	 * @param mail Mail dell'utente loggato
	 * @param prodotto Prodotto da cancellare 
	 * */
	@Transactional
	public boolean deleteProdotto(String mail, List<WrapperProdottoUtente> prodotto) {
		
		Utente user = utentiDB.findByMail(mail);
		
		if(user == null) {
			return false;
		}
		
		
		if (prodotto.get(0).getProdotto().equalsIgnoreCase("vino")) {
			Vino vino = viniDB.findById(Long.parseLong(prodotto.get(0).getIdProdotto())).get();
			
			if(vino==null) {
				return false;
			}
			
			UtenteVino user_has_vino = utentiViniDB.findByUtenteAndAnnoAndVino(
					user,
					Integer.parseInt(prodotto.get(0).getAnno()),
					vino);	
			
			if(user_has_vino==null) {
				return false;
			}
			
			
			utentiViniDB.delete(user_has_vino);
		}else
			if(prodotto.get(0).getProdotto().equalsIgnoreCase("grappa")) {
				Grappa grappa = grappaDB.findById(Long.parseLong(prodotto.get(0).getIdProdotto())).get();
				
				if(grappa==null) {
					return false;
				}
				
				UtenteGrappa user_has_grappa = utenteGrappaDB.findByUtenteAndAnnoAndGrappa(
						user,
						Integer.parseInt(prodotto.get(0).getAnno()),
						grappa);
				
				if(user_has_grappa==null) {
					return false;
				}
				
				utenteGrappaDB.delete(user_has_grappa);
			}	
		return true;
		
	}
	
	/**
	 * Metodo per l'aggiunta del vino alla cantina di un utente
	 * @param model
	 * @param utente Utente che sta aggiungendo vino alla sua cantina
	 * @param nomevino Nome del vino
	 * @param anno Anno del vino
	 * @param nbottiglie Numero di bottiglie di vino aggiunte
	 */
	@Transactional
	public void addVinoToCantina(Model model, String mail, String nomevino, String anno, String nbottiglie, String cantinaString) {
		
		
		Utente utente = utentiDB.findByMail(mail);
		CantinaProduttrice cantinaProd = cantinaDB.findByNome(cantinaString);
		if(cantinaProd==null){
			setError(model, "Cantina non corretta.");
			return;
		}
		
		Vino vino = viniDB.findByNomeAndIdCantina(nomevino, cantinaProd);
		if(vino==null){
			setError(model, "Vino non corretto.");
			return;
		}else{
			UtenteVino cantina = utentiViniDB.findByUtenteAndAnnoAndVino(utente, Integer.parseInt(anno), vino);
			if(cantina == null) {
				UtenteVino tmp = new UtenteVino(utente, vino, Integer.parseInt(anno), Integer.parseInt(nbottiglie));
				utentiViniDB.saveAndFlush(tmp);
			}else{
				cantina.setnBottiglie(cantina.getnBottiglie()+Integer.parseInt(nbottiglie));
				model.addAttribute("response", "success");
			}			
		}
		
	}
	
	/**
	 * Metodo per l'aggiunta della grappa alla cantina di un utente
	 * @param model
	 * @param utente Utente che sta aggiungendo grappa alla sua cantina
	 * @param nomevino Nome della grappa
	 * @param anno Anno della grappa
	 * @param nbottiglie Numero di bottiglie di vino aggiunte
	 */
	@Transactional
	public void addToGrappacellar (Model model, String mail, String nomegrappa, String anno, String nbottiglie, String cantinaString) {
		
		Utente utente = utentiDB.findByMail(mail);
		
		CantinaProduttrice cantinaProd = cantinaDB.findByNome(cantinaString);
		if(cantinaProd==null){
			setError(model, "Cantina non corretta.");
			return;
		}
		
		Grappa grappa = grappaDB.findByNomeAndIdCantina(nomegrappa, cantinaProd);
		if(grappa==null){
			setError(model, "Grappa non corretta.");
			return;
		}
		UtenteGrappa cantina = utenteGrappaDB.findByUtenteAndAnnoAndGrappa(utente, Integer.parseInt(anno), grappa);
		if(cantina == null) {
			UtenteGrappa tmp = new UtenteGrappa(utente, grappa, Integer.parseInt(anno), Integer.parseInt(nbottiglie));
			utenteGrappaDB.save(tmp);
			utenteGrappaDB.flush();
		}else{
			cantina.setnBottiglie(cantina.getnBottiglie()+Integer.parseInt(nbottiglie));
			model.addAttribute("response", "success");
		}			
	}
	
    public final void setError(Model model, String message) {
		model.addAttribute("error", true);
		model.addAttribute("message", message);
	}
    
    public boolean checkPermission(String mail) {
    	if(!utentiDB.existsByMail(mail)) {
    		return false;
    	}else {
    		return true;
    	}
    	
    }
}

