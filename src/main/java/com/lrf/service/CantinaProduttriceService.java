package com.lrf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import com.lrf.domain.CantinaProduttrice;
import com.lrf.repository.CantinaProduttriceRepository;

@Service
public class CantinaProduttriceService {
	
	@Autowired
	CantinaProduttriceRepository cantineProdDB;
	
	/**
	 * Metodo per l'aggiunta di una cantina produttrice
	 * @param model
	 * @param nome
	 */
	@Transactional
	public boolean addCantinaProduttrice(Model model, String nome) {
		
		if(!cantineProdDB.existsByNome(nome)) {
			CantinaProduttrice cantina = new CantinaProduttrice(nome);
			cantineProdDB.save(cantina);
			model.addAttribute("response","Cantina aggiunta correttamente.");
			findAll(model);
			return true;
		}else {
			setError(model,"Cantina già esistente.");
			findAll(model);
			return false;
		}
		
	}
	
	/**
	 * Metodo per la delete di una cantina produttrice
	 * @param model
	 * @param idString
	 * @return
	 */
	@Transactional
	public String deleteCantinaProduttrice(Model model, String idString) {
		
		Long id = Long.parseLong(idString);
		if(cantineProdDB.existsById(id)) {
			CantinaProduttrice cantina = cantineProdDB.findById(id).get();
			if(cantina.getProdotti()!= null &&
					cantina.getProdotti().size() > 0) {
				findAll(model);
				return "falseAssociazione";
			}else {
				cantineProdDB.deleteById(id);
				findAll(model);
				model.addAttribute("response","Cantina eliminata correttamente.");
			}			
		} else {
			return "false";
		}
		return "true";
	}
	
	/**
	 * Metodo per l'update di una cantina produttrice
	 * @param model
	 * @param oldName
	 * @param newName
	 */
	@Transactional
	public void updateCantinaProduttrice(Model model, String oldName, String newName) {		
		
		CantinaProduttrice cantina = cantineProdDB.findByNome(oldName);
		if (cantina ==null){
			setError(model,"Cantina non trovata.");
		}else {
			
		CantinaProduttrice newCantina = cantineProdDB.findByNome(newName);
			if(newCantina == null) {
				cantina.setNome(newName);
				cantineProdDB.save(cantina);
			}else {
				setError(model,"Cantina già esistente.");
			}
		}
		findAll(model);
		
	}
	
	/**
	 * Metodo per la get di tutte le cantine produttrici
	 * @param model
	 */
	public void findAll(Model model) {
		model.addAttribute("cantine", cantineProdDB.findAll());
	}
	
	/**
	 * Metodo per settare messaggi d'errore
	 * @param model
	 * @param message
	 */
	public void setError(Model model, String message) {
		model.addAttribute("error", true);
		model.addAttribute("message", message);
	}
	
}
