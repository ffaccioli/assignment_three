package com.lrf.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.lrf.service.CantinaProduttriceService;
import com.lrf.service.ProdottoService;
import com.lrf.service.UtenteService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	public ProdottoService appServiceInstantiation() {
		return new ProdottoService();
	}
	
	@Autowired
	public UtenteService utentepServiceInstantiation() {
		return new UtenteService();
	}
	
	@Autowired
    public CantinaProduttriceService cantinaProdServiceInstantiation() {
        return new CantinaProduttriceService();
    }

   @Bean
   public PasswordEncoder passwordEncoder() {
       return new BCryptPasswordEncoder();
   }
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//TODO: provare a non disabilitare CRSF e CORS ma metterli nell'Header
	    	http
	        .cors()
	            .and()
	        .csrf()
	            .disable()
			.authorizeRequests()
				.antMatchers("/**", "/home", "/vino**", "/grappa**", "/cantina-produttrice**").permitAll()
				.and()
				.logout().logoutSuccessUrl("/home").deleteCookies("Mail","Name","Lastname","Logged","Role")
                .invalidateHttpSession(true)
                //.clearAuthentication(true)
				.permitAll();
	}
	
}
