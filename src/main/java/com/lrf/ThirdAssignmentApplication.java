package com.lrf;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.lrf.domain.CantinaProduttrice;
import com.lrf.domain.UtenteVino;
import com.lrf.domain.Grappa;
import com.lrf.domain.Prodotto;
import com.lrf.domain.Regione;
import com.lrf.domain.Utente;
import com.lrf.domain.Vino;
import com.lrf.service.ProdottoService;
import com.lrf.service.UtenteService;

@SpringBootApplication
@EntityScan(basePackageClasses={ 
		ThirdAssignmentApplication.class, Jsr310JpaConverters.class,
		Utente.class, Vino.class, Grappa.class, ProdottoService.class, UtenteService.class,
		Prodotto.class, Regione.class, CantinaProduttrice.class, UtenteVino.class
})
public class ThirdAssignmentApplication {
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@PostConstruct
	void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}
	
	@PostConstruct
	public void setUp() {
	   objectMapper.registerModule(new JavaTimeModule());
	}

	public static void main(String[] args) {
		SpringApplication.run(ThirdAssignmentApplication.class, args);
	}

}
