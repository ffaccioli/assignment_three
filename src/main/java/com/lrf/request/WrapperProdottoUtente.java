package com.lrf.request;

public class WrapperProdottoUtente {

	private String anno;
	private String idUtente;
	private String idProdotto;
	private String nBottiglie;
	private String prodotto;
	
	
	
	
	public WrapperProdottoUtente(String anno, String idUtente, String idProdotto, String nBottiglie, String prodotto) {
		super();
		this.anno = anno;
		this.idUtente = idUtente;
		this.idProdotto = idProdotto;
		this.nBottiglie = nBottiglie;
		this.prodotto = prodotto;
	}
	
	public String getProdotto() {
		return prodotto;
	}

	public void setProdotto(String prodotto) {
		this.prodotto = prodotto;
	}

	public String getAnno() {
		return anno;
	}
	public void setAnno(String anno) {
		this.anno = anno;
	}
	public String getIdUtente() {
		return idUtente;
	}
	public void setIdUtente(String idUtente) {
		this.idUtente = idUtente;
	}
	public String getIdProdotto() {
		return idProdotto;
	}
	public void setIdProdotto(String idProdotto) {
		this.idProdotto = idProdotto;
	}
	public String getnBottiglie() {
		return nBottiglie;
	}
	public void setnBottiglie(String nBottiglie) {
		this.nBottiglie = nBottiglie;
	}
	
	

}
