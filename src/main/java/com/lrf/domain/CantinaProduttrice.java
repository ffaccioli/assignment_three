package com.lrf.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="cantina")
public class CantinaProduttrice {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idc")
	private Long idc;
	
	@NotBlank
	@Column(name="NomeCantina", unique = true)
	private String nome;
	
	@JsonIgnore
	@OneToMany(mappedBy = "idCantina")
	private List<Prodotto> prodotti;
	
	public CantinaProduttrice() {}
	
	public CantinaProduttrice(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Long getIdc() {
		return idc;
	}

	public void setIdc(Long idc) {
		this.idc = idc;
	}

	public List<Prodotto> getProdotti() {
		return prodotti;
	}

	public void setProdotti(List<Prodotto> prodotti) {
		this.prodotti = prodotti;
	}
	
	@JsonIgnore
	public List<Vino> getVini() {
		List<Vino> lista = new ArrayList<Vino>();
		
		for (Prodotto prodotto : prodotti) {
			if(prodotto instanceof Vino) {
				lista.add((Vino) prodotto);
			}
		}
		return lista;
	}
	
	@JsonIgnore
	public List<Grappa> getGrappa() {
		List<Grappa> lista = new ArrayList<Grappa>();
		
		for (Prodotto prodotto : prodotti) {
			if(prodotto instanceof Grappa) {
				lista.add((Grappa) prodotto);
			}
		}
		return lista;
	}
	
}