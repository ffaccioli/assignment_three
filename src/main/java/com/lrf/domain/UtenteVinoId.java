package com.lrf.domain;

import java.io.Serializable;

public class UtenteVinoId implements Serializable {

	private Long utente;
	private Long vino;
	private int anno;
	
	public Long getUtente() {
		return utente;
	}
	public void setUtente(Long utente) {
		this.utente = utente;
	}
	public Long getVino() {
		return vino;
	}
	public void setVino(Long vino) {
		this.vino = vino;
	}
	public int getAnno() {
		return anno;
	}
	public void setAnno(int anno) {
		this.anno = anno;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + anno;
		result = prime * result + ((utente == null) ? 0 : utente.hashCode());
		result = prime * result + ((vino == null) ? 0 : vino.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UtenteVinoId other = (UtenteVinoId) obj;
		if (anno != other.anno)
			return false;
		if (utente == null) {
			if (other.utente != null)
				return false;
		} else if (!utente.equals(other.utente))
			return false;
		if (vino == null) {
			if (other.vino != null)
				return false;
		} else if (!vino.equals(other.vino))
			return false;
		return true;
	}
	
}
