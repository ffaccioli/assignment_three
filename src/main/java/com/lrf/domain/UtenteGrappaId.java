package com.lrf.domain;

import java.io.Serializable;

public class UtenteGrappaId implements Serializable{
	
	private Long utente;
	private Long grappa;
	private int anno;
	
	public Long getUtente() {
		return utente;
	}
	
	public void setUtente(Long utente) {
		this.utente = utente;
	}
	
	public Long getGrappa() {
		return grappa;
	}
	
	public void setGrappa(Long grappa) {
		this.grappa = grappa;
	}
	
	public int getAnno() {
		return anno;
	}
	
	public void setAnno(int anno) {
		this.anno = anno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + anno;
		result = prime * result + ((grappa == null) ? 0 : grappa.hashCode());
		result = prime * result + ((utente == null) ? 0 : utente.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UtenteGrappaId other = (UtenteGrappaId) obj;
		if (anno != other.anno)
			return false;
		if (grappa == null) {
			if (other.grappa != null)
				return false;
		} else if (!grappa.equals(other.grappa))
			return false;
		if (utente == null) {
			if (other.utente != null)
				return false;
		} else if (!utente.equals(other.utente))
			return false;
		return true;
	}
	
	

	

}
