package com.lrf.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "utente_has_grappa" )
@IdClass(UtenteGrappaId.class)
public class UtenteGrappa {


	@Id
	@ManyToOne
	@JoinColumn(name = "grappa_id", referencedColumnName = "idprodotto")
	private Grappa grappa;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "utente_id", referencedColumnName = "id")
	private Utente utente;
	
	@Id
	@Column(name = "anno")
	private int anno;
	
	
	@Column(name = "nBottiglie")
	private int nBottiglie;
	
	public UtenteGrappa() {}
	
	public UtenteGrappa(Utente utente, Grappa grappa, int anno, int n) {
		setGrappa(grappa);
		setUtente(utente);
		setAnno(anno);
		setnBottiglie(n);
	}

	public Grappa getGrappa() {
		return grappa;
	}

	public void setGrappa(Grappa grappa) {
		this.grappa = grappa;
	}

	public Utente getUtente() {
		return utente;
	}

	public void setUtente(Utente utente) {
		this.utente = utente;
	}

	public int getAnno() {
		return anno;
	}

	public void setAnno(int anno) {
		this.anno = anno;
	}

	public int getnBottiglie() {
		return nBottiglie;
	}

	public void setnBottiglie(int nBottiglie) {
		this.nBottiglie = nBottiglie;
	}
	
}
