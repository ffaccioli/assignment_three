package com.lrf.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "utente_has_vino" )
@IdClass(UtenteVinoId.class)
public class UtenteVino {

	@Id
	@ManyToOne
	@JoinColumn(name = "vino_id", referencedColumnName = "idprodotto")
	private Vino vino;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "utente_id", referencedColumnName = "id")
	private Utente utente;
	
	@Id
	@Column(name = "anno")
	private int anno;
	
	
	@Column(name = "nBottiglie")
	private int nBottiglie;
	
	public UtenteVino() {}
	
	public UtenteVino(Utente utente, Vino vino, int anno, int n) {
		setVino(vino);
		setUtente(utente);
		setAnno(anno);
		setnBottiglie(n);
	}

	public Vino getVino() {
		return vino;
	}

	public void setVino(Vino vino) {
		this.vino = vino;
	}

	public Utente getUtente() {
		return utente;
	}

	public void setUtente(Utente utente) {
		this.utente = utente;
	}

	public int getAnno() {
		return anno;
	}

	public void setAnno(int anno) {
		this.anno = anno;
	}

	public int getnBottiglie() {
		return nBottiglie;
	}

	public void setnBottiglie(int nBottiglie) {
		this.nBottiglie = nBottiglie;
	}
	
}
