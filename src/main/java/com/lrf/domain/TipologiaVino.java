package com.lrf.domain;

public enum TipologiaVino {
	ROSSO,
    BIANCO;
}