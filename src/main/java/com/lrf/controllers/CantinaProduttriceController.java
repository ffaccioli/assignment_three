package com.lrf.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.lrf.service.CantinaProduttriceService;

@Controller
@RequestMapping("/cantina-produttrice")
public class CantinaProduttriceController {
	
	@Autowired
	private CantinaProduttriceService service;
	
	@GetMapping("")
	public String listAllCantine(Model model) {
		service.findAll(model);
		return "cantineProd";
	}
	
	@RequestMapping(value="/edit", method=RequestMethod.POST)
	public String editCantinaProduttrice(@RequestParam("oldName") String oldName,
			@RequestParam("editNome") String newName, Model model) {
		service.updateCantinaProduttrice(model, oldName, newName);		
		return "cantineProd";		
	}
	
	@PostMapping("/add")
	public @ResponseBody boolean createCantinaProduttrice(@RequestBody String nome, Model model) {
		return service.addCantinaProduttrice(model, nome);
	}
	
	@PostMapping("/delete")
	public @ResponseBody String deleteCantinaProduttrice(@RequestBody String idString, Model model) {
		return service.deleteCantinaProduttrice(model, idString);
	}
	
}