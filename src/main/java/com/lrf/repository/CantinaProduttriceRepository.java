package com.lrf.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.lrf.domain.CantinaProduttrice;

public interface CantinaProduttriceRepository extends JpaRepository<CantinaProduttrice, Long> {
	
	boolean existsByNome(String nome);
	CantinaProduttrice findByNome(String nome);
		
}
