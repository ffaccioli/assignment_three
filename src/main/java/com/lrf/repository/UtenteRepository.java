package com.lrf.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lrf.domain.Utente;

public interface UtenteRepository extends JpaRepository<Utente, Long> {
	
	boolean existsByCognome(String cognome);
	List<Utente> findByNome(String nome);
	Utente findByMail(String mail);
	Optional<Utente> findById(Long id);
	boolean existsByMail(String mail);
}
