package com.lrf.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lrf.domain.Grappa;
import com.lrf.domain.Utente;
import com.lrf.domain.UtenteGrappa;

public interface UtenteGrappaRepository extends JpaRepository<UtenteGrappa, Long> {
	
	boolean existsById(Long id);
	Optional<UtenteGrappa> findById(Long id);
	List<UtenteGrappa> findByAnno(int anno);
	List<UtenteGrappa> findByGrappa(Grappa grappa);
	List<UtenteGrappa> findByUtente(Utente utente);
	UtenteGrappa findByUtenteAndAnnoAndGrappa(Utente user, int Anno, Grappa grappa);

}
