package com.lrf.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lrf.domain.CantinaProduttrice;
import com.lrf.domain.Grappa;
import com.lrf.domain.TipologiaGrappa;

@Repository
public interface GrappaRepository extends JpaRepository<Grappa, Long> {
	
	boolean existsById(Long id);
	boolean existsByTipologiaGrappa(Enum<TipologiaGrappa> tipologia);
	List<Grappa> findByTipologiaGrappa(Enum<TipologiaGrappa> tipologia);
	Grappa findByNome(String nome);
	Optional<Grappa> findById(Long Id);
	Grappa findByNomeAndIdCantina(String nome, CantinaProduttrice cantina);

}