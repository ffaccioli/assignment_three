package com.lrf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.lrf.domain.Prodotto;

public interface ProdottoRepository  extends JpaRepository<Prodotto, Long> {

	boolean existsByNome(String nome);
	List<Prodotto> findByNome(String nome);
	List<Prodotto> findByNomeAndDenominazioneAndNazionalita(String nome, String denominazione, String Nazionalita);
}
